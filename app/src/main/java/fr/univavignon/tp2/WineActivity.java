package fr.univavignon.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class WineActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        Intent intent = getIntent();
        Wine wine = (Wine) intent.getSerializableExtra("wineChosen");



        EditText editTitle = findViewById(R.id.wineName);
        EditText editWineRegion = findViewById(R.id.editWineRegion);
        EditText editLoc = findViewById(R.id.editLoc);
        EditText editClimate = findViewById(R.id.editClimate);
        EditText editPlantedArea = findViewById(R.id.editPlantedArea);
        Button button = findViewById(R.id.button);


        /*editTitle.setText(wine.getTitle());
        editWineRegion.setText(wine.getRegion());
        editLoc.setText(wine.getLocalization());
        editClimate.setText(wine.getClimate());
        editPlantedArea.setText(wine.getPlantedArea());*/



        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(WineActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });


    }
}
